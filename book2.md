# Book 2: "1984"

- Author: George Orwell
- Published: 1949
- Genre: Science Fiction, Dystopia
- Description: A classic dystopian novel depicting a totalitarian society where truth is manipulated, surveillance is omnipresent, and individual freedom is under control.

# Book 3: "The Lord of the Rings: The Fellowship of the Ring"

- Author: J.R.R. Tolkien
- Published: 1954
- Genre: Fantasy, Adventure
- Description: The first volume of an epic trilogy that follows the quest of a young hobbit, Frodo Baggins, to destroy an evil ring and save Middle-earth from malevolent domination.
